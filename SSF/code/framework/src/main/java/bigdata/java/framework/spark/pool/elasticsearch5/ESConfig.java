/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.elasticsearch5;

import bigdata.java.framework.spark.pool.ConnectionPoolConfig;
import com.typesafe.config.Config;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class ESConfig extends ConnectionPoolConfig {

    Properties properties = new Properties();
    Properties poolProperties = new Properties();
    public ESConfig()
    {
        init();
    }
    public void init()
    {
        Config config = getConfig();
        String nd = config.getString("es.nodes");
        String[] split = nd.split(",");
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            String[] serverAndPort = s.split(":");
            String ip = serverAndPort[0];
            Integer port = Integer.parseInt(serverAndPort[1]);
            nodes.add(new HostAndPort(ip,port,"http"));
        }
        properties = ConnectionPoolConfig.getProperties("esparam.");
        poolProperties = ConnectionPoolConfig.getProperties("es.pool.");
        ConnectionPoolConfig.setPoolProperties(this,poolProperties);
    }

    public Properties getPoolProperties() {
        return poolProperties;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    Set<HostAndPort> nodes = new HashSet<HostAndPort>();
    public Set<HostAndPort> getNodes() {
        return nodes;
    }

    public void setNodes(Set<HostAndPort> nodes) {
        this.nodes = nodes;
    }
}
