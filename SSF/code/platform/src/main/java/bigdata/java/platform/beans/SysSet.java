package bigdata.java.platform.beans;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SysSet {

    @Value("${SysSet.sparkApplication}")
    String sparkApplication;

    @Value("${SysSet.livy}")
    String livy;

    @Value("${SysSet.intervalTime}")
    Integer intervalTime;

    @Value("${SysSet.hdfsAccount}")
    String hdfsAccount;

    @Value("${SysSet.yarn.web}")
    String yarn;

    public String getShellScript() {
        return shellScript;
    }

    public void setShellScript(String shellScript) {
        this.shellScript = shellScript;
    }

    @Value("${yarn.logs.download.shellscript}")
    String shellScript;

    public Integer getTaskChartIntervalTime() {
        return taskChartIntervalTime;
    }

    public void setTaskChartIntervalTime(Integer taskChartIntervalTime) {
        this.taskChartIntervalTime = taskChartIntervalTime;
    }

    @Value("${SysSet.taskChart.intervalTime}")
    Integer taskChartIntervalTime;

    String defaultJars;

    String defaultFiles;

    String conf;

    String param;

    String args;

    Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getYarn() {
        return yarn;
    }

    public void setYarn(String yarn) {
        this.yarn = yarn;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

//    @Value("${SysSet.queueName}")
//    String queueName;

    String queueName;
    public Integer getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(Integer intervalTime) {
        this.intervalTime = intervalTime;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getSparkApplication() {
        return sparkApplication;
    }

    public void setSparkApplication(String sparkApplication) {
        this.sparkApplication = sparkApplication;
    }

    public String getLivy() {
        return livy;
    }

    public void setLivy(String livy) {
        this.livy = livy;
    }

    public String getHdfsAccount() {
        return hdfsAccount;
    }

    public void setHdfsAccount(String hdfsAccount) {
        this.hdfsAccount = hdfsAccount;
    }

    public String getDefaultJars() {
        return defaultJars;
    }

    public void setDefaultJars(String defaultJars) {
        this.defaultJars = defaultJars;
    }

    public String getDefaultFiles() {
        return defaultFiles;
    }

    public void setDefaultFiles(String defaultFiles) {
        this.defaultFiles = defaultFiles;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
