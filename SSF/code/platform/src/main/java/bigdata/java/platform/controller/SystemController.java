package bigdata.java.platform.controller;

import bigdata.java.platform.beans.TFile;
import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.core.Permission;
import bigdata.java.platform.service.FileService;
import bigdata.java.platform.service.SystemService;
import bigdata.java.platform.util.Comm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/system/")
public class SystemController {

    @Autowired
    SystemService systemService;

    @Autowired
    FileService fileService;

    @Autowired
    SysSet sysSet;

    @GetMapping("sysset")
    @Permission
    public ModelAndView getSystem()
    {
        ModelAndView mv = new ModelAndView();
        SysSet sysSetModel = systemService.get(Comm.getUserId());
//        sysSetModel.setSparkApplication(sysSet.getSparkApplication());
//        sysSetModel.setQueueName(sysSet.getQueueName());
//        sysSetModel.setHdfsAccount(sysSet.getHdfsAccount());
//        sysSetModel.setIntervalTime(sysSet.getIntervalTime());
//        sysSetModel.setLivy(sysSet.getLivy());
        mv.addObject("model",sysSetModel);
        List<TFile> list = fileService.list(Comm.getUserId());
        mv.addObject("files",list);
        mv.setViewName("sysset");
        return mv;
    }

    @PostMapping("sysset")
    @Permission
    public ModelAndView doSystem(SysSet sysSetModel)
    {
        ModelAndView mv = new ModelAndView();
        sysSetModel.setUserId(Comm.getUserId());
        systemService.edit(sysSetModel);
//        sysSetModel.setSparkApplication(sysSet.getSparkApplication());
//        sysSetModel.setQueueName(sysSet.getQueueName());
//        sysSetModel.setHdfsAccount(sysSet.getHdfsAccount());
//        sysSetModel.setIntervalTime(sysSet.getIntervalTime());
//        sysSetModel.setLivy(sysSet.getLivy());
        mv.addObject("model",sysSetModel);
//        mv.setViewName("sysset");
        mv.setViewName("redirect:sysset");
        return mv;
    }
}
