package bigdata.java.platform.core;

import bigdata.java.platform.beans.KafkaOffset;
import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.TaskChart;
import bigdata.java.platform.service.TaskChartService;
import bigdata.java.platform.service.TaskService;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@DisallowConcurrentExecution
public class ScheduledTaskChart extends QuartzJobBean {

    static final String FORMAT_YYYY_MM_DDHHMM="yyyy-MM-dd HH:mm";
    static final TimeZone timeZone = TimeZone.getTimeZone("Asia/Shanghai");
    static final FastDateFormat DateFormatYMDHM = FastDateFormat.getInstance(FORMAT_YYYY_MM_DDHHMM,timeZone);
    static final ExecutorService threadPool = Executors.newFixedThreadPool(6);
    @Autowired
    TaskService taskService;

    @Autowired
    TaskChartService taskChartService;
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        List<TTask> tTasks = taskService.list();
        if(tTasks==null || tTasks.size() < 1)
        {
            return ;
        }

        Stream<TTask> tTaskStream = tTasks.stream().filter(e -> !StringUtils.isBlank(e.getOpenChart()) && e.getOpenChart().equals("1")  && e.getStatus().intValue()==20);
        if(tTaskStream==null)
        {
            return ;
        }
        List<TTask> collect = tTaskStream.collect(Collectors.toList());

        if(collect==null || collect.size() < 1)
        {
            return ;
        }

        Date date = new Date();
        String format = DateFormatYMDHM.format(date);
        CountDownLatch countDownLatch = new CountDownLatch(collect.size());
        for (int i = 0; i < collect.size(); i++) {
            TTask task = collect.get(i);
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try{
                        exec(task,format);
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException(e);
                    }
                    finally {
                        countDownLatch.countDown();
                    }
                }
            });
        }

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(TTask task,String format)
    {
        Map<String,TaskChart> taskChartMap = new HashMap<String,TaskChart>();
        List<String> sqlList = new ArrayList<>();
        List<KafkaOffset> offsetList = taskService.getOffset(task.getUserId(),task.getTaskId());
        for (int i = 0; i < offsetList.size(); i++) {
            KafkaOffset offset = offsetList.get(i);
            TaskChart taskChart = taskChartMap.get(offset.getTopic());
            if(taskChart ==null)
            {
                taskChart = new TaskChart();
                taskChart.setTaskId(task.getTaskId());
                taskChart.setTopic(offset.getTopic());
                taskChart.setConsumeOffset(Long.parseLong(offset.getCurrentOffset()));
                taskChart.setMaxOffset(Long.parseLong(offset.getMaxOffset()));
                taskChartMap.put(offset.getTopic(),taskChart);
            }
            else
            {
                long currentOffset = Long.parseLong(offset.getCurrentOffset());
                long maxOffset = Long.parseLong(offset.getMaxOffset());
                taskChart.setConsumeOffset(taskChart.getConsumeOffset() + currentOffset);
                taskChart.setMaxOffset(taskChart.getMaxOffset() + maxOffset);
            }
//            taskChart.setUptime(format);
        }

        for (Map.Entry<String,TaskChart> taskChart : taskChartMap.entrySet())
        {
            TaskChart value = taskChart.getValue();
            String sql = "insert  into TaskChart (taskId,topic,consumeOffset,maxOffset,uptime)values("+value.getTaskId()+",'"+value.getTopic()+"',"+value.getConsumeOffset()+","+value.getMaxOffset()+",'"+format+"')";
            sqlList.add(sql);
        }
        taskChartService.add(sqlList);
    }
}
