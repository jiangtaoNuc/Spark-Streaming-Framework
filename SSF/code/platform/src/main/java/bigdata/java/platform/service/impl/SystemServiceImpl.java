package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.service.SystemService;
import bigdata.java.platform.util.Comm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

@Service
public class SystemServiceImpl implements SystemService {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public SysSet get(Integer userId) {
        String sql = "select * from SysSet where userId=:userId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", userId);
        SysSet sysSet = namedParameterJdbcTemplate.queryForObject(sql, parameters, SysSet.class);
        return sysSet;
    }

    @Override
    public Boolean edit(SysSet sysSet) {
        //String sql = "edit SysSet set sparkApplication=:sparkApplication,livy=:livy,intervalTime=:intervalTime,hdfsAccount=:hdfsAccount,defualtJars=:defualtJars,defaultFiles=:defaultFiles,queueName=:queueName";
        String sql;

        if(sysSet.getQueueName()==null)
        {
            sql = "update SysSet set defaultJars=:defaultJars,defaultFiles=:defaultFiles,conf=:conf,param=:param,args=:args where userId=:userId";
        }
        else
        {
            sql = "update SysSet set defaultJars=:defaultJars,defaultFiles=:defaultFiles,conf=:conf,param=:param,args=:args,queueName=:queueName where userId=:userId";
        }

        SqlParameterSource parameters = new BeanPropertySqlParameterSource(sysSet);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public Boolean add(SysSet sysSet) {
        String sql = "insert into SysSet (conf,param,args,defaultJars,defaultFiles,queueName,userId)values(:conf,:param,:args,:defaultJars,:defaultFiles,:queueName,:userId)";
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(sysSet);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }
}
